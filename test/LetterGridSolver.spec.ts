/// <reference path="../typings/index.d.ts" />

import {LetterGridSolver, GridSearchResult} from "../app/utils/LetterGridSolver";
import {LetterGrid} from "../app/utils/LetterGrid";
import {expect} from "chai";

describe('LetterGridResolver', () => {
  var solver:LetterGridSolver;

  beforeEach(function () {
    solver = new LetterGridSolver();
  });

  describe('Example letter grid', () => {

    it('should match answer', () => {
      let letterGridStr =
        'CATZ\n'
        + 'AGOD\n'
        + 'tnzz\n'
        + 'zztz';
      let words = ['Cat', 'Dog', 'fish'];
      solver.setLetterGrid(new LetterGrid(letterGridStr));
      solver.setWords(words);

      let results:GridSearchResult[] = solver.solve();

      expect(results.sort((a, b)=>a.word.localeCompare(b.word))).eql([
        {
          "word": "cat",
          "startX": 0,
          "startY": 0,
          "endX": 2,
          "endY": 0
        },
        {
          "word": "dog",
          "startX": 3,
          "startY": 1,
          "endX": 1,
          "endY": 1
        }].sort((a, b)=>a.word.localeCompare(b.word)));
    });
  });

  describe('Nice and Simple letter grid', () => {

    it('should match answer', () => {
      let letterGridStr =
        'CIRN\n'
        + 'Adog\n'
        + 'tcis\n'
        + 'kcow';
      let words = ['CAT', 'DOG', 'COW'];
      solver.setLetterGrid(new LetterGrid(letterGridStr));
      solver.setWords(words);

      let results:GridSearchResult[] = solver.solve();

      expect(results.sort((a, b)=>a.word.localeCompare(b.word))).eql([
        {
          "word": "cat",
          "startX": 0,
          "startY": 0,
          "endX": 0,
          "endY": 2
        },
        {
          "word": "dog",
          "startX": 1,
          "startY": 1,
          "endX": 3,
          "endY": 1
        },
        {
          "word": "cow",
          "startX": 1,
          "startY": 3,
          "endX": 3,
          "endY": 3
        }
      ].sort((a, b)=> a.word.localeCompare(b.word)));
    });
  });

  describe('Words may appear forwards or backwards', () => {

    it('should match answer', () => {
      let letterGridStr =
          'DNOMAID\n'
          + 'PQINEEG\n'
          + 'XXWQTDK\n'
          + 'CDKBRAF\n'
          + 'UWERAFX\n'
          + 'TDAFESJ\n'
          + 'AKJSHHE'
        ;
      let words = ['diamond', 'heart'];
      solver.setLetterGrid(new LetterGrid(letterGridStr));
      solver.setWords(words);

      let results:GridSearchResult[] = solver.solve();

      expect(results.sort((a, b)=>a.word.localeCompare(b.word))).eql([
        {
          "word": "diamond",
          "startX": 6,
          "startY": 0,
          "endX": 0,
          "endY": 0
        },
        {
          "word": "heart",
          "startX": 4,
          "startY": 6,
          "endX": 4,
          "endY": 2
        }
      ].sort((a, b)=> a.word.localeCompare(b.word)));
    });
  });

  describe('Missing words', () => {

    it('should match answer', () => {
      let letterGridStr =
          'CIRN\n'
          + 'ADOG\n'
          + 'TCIS\n'
          + 'KDIE'
        ;
      let words = ['cat', 'dog', 'duck'];
      solver.setLetterGrid(new LetterGrid(letterGridStr));
      solver.setWords(words);

      let results:GridSearchResult[] = solver.solve();

      expect(results.sort((a, b)=>a.word.localeCompare(b.word))).eql([
        {
          "word": "cat",
          "startX": 0,
          "startY": 0,
          "endX": 0,
          "endY": 2
        },
        {
          "word": "dog",
          "startX": 1,
          "startY": 1,
          "endX": 3,
          "endY": 1
        }
      ].sort((a, b)=> a.word.localeCompare(b.word)));
    });
  });

  describe('A Crazy letter grid', () => {

    it('should match answer', () => {
      let size = 200;

      let randomString = (len:number) => {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < len; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
      };

      //big matrix
      let arr:string[] = [];
      for(let i=0;i<size;i++){
        arr.push(randomString(size));
      }
      let letterGridStr = arr.join('\n');

      //big word list
      let words = [];
      for(let i=0;i<size;i++){
        words.push(randomString(i+3));
      }

      solver.setLetterGrid(new LetterGrid(letterGridStr));
      solver.setWords(words);

      let results:GridSearchResult[] = solver.solve();

    });
  });

});
