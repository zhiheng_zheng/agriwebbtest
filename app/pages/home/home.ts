import {Component} from "@angular/core";
import {NavController, AlertController} from "ionic-angular";
import {LetterGrid} from "../../utils/LetterGrid";
import {LetterGridSolver, GridSearchResult} from "../../utils/LetterGridSolver";
import {ResultPage} from "../result/result";

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  words:string[];
  letterGridStr:string;
  solver:LetterGridSolver;


  constructor(public navCtrl:NavController,
              private alertCtrl:AlertController) {
    this.words = [];
  }

  addWord(word:string) {
    if (word) {
      if (!this.words.find(it => it === word))
        this.words.push(word);
    }
  }

  presentAddWordAlert() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Add a Word');

    alert.addInput({
      type: 'text',
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.addWord(data[0]);
      }
    });
    alert.present();
  }

  removeWord(word:string) {
    this.words.splice(this.words.findIndex(it => it === word), 1);
  }

  solve() {
    if (!this.words || this.words.length === 0) {
      let alert = this.alertCtrl.create({
        title: 'Empty word list',
        subTitle: 'Please add some words',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    if (!this.letterGridStr) {
      let alert = this.alertCtrl.create({
        title: 'Empty letter grid',
        subTitle: 'Please input letter grid',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    let letterGrid:LetterGrid = new LetterGrid(this.letterGridStr);
    if (!letterGrid.isValid()) {
      let alert = this.alertCtrl.create({
        title: 'Invalid letter grid',
        subTitle: 'It should have the same number of rows and columns',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    this.solver = new LetterGridSolver();
    this.solver.setLetterGrid(letterGrid);
    this.solver.setWords(this.words);
    let results:GridSearchResult[] = this.solver.solve();
    this.navCtrl.push(ResultPage, {'results': results, 'words': this.words});
  }

}
