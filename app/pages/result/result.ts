import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {HomePage} from "../home/home";
import {GridSearchResult} from "../../utils/LetterGridSolver";

@Component({
  templateUrl: 'build/pages/result/result.html',
})
export class ResultPage {

  results:GridSearchResult[];
  words:string[];
  mapping:Map<string,GridSearchResult>;

  constructor(private navCtrl:NavController,
              private params:NavParams) {
    this.results = this.params.get('results');
    this.words = this.params.get('words');
    this.mapping = new Map<string, GridSearchResult>();
    this.results.forEach((it:GridSearchResult)=> {
      this.mapping.set(it.word.toLowerCase(), it);
    });
  }

  newPuzzle() {
    this.navCtrl.setRoot(HomePage);
  }

  startString(word:string) {
    let searchResult:GridSearchResult;
    searchResult = this.mapping.get(word.toLowerCase());
    if (searchResult) {
      return `(${searchResult.startX + 1},${searchResult.startY + 1})`;
    } else
      return '-';
  }

  endString(word:string) {
    let searchResult:GridSearchResult;
    searchResult = this.mapping.get(word.toLowerCase());
    if (searchResult) {
      return `(${searchResult.endX + 1},${searchResult.endY + 1})`;
    } else
      return '-';
  }

}
