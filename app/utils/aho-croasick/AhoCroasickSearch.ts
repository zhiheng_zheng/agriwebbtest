/**
 * Implement Aho-Corasick algorithm based on the description
 * in the https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm
 */
export class AhoCroasickSearch {
  private words:Set<string>;
  private root:TrieNode;
  private allNodes:Map<string, TrieNode>;

  constructor() {
    this.root = new TrieNode();
    this.allNodes = new Map<string, TrieNode>();
    this.allNodes.set(this.root.value, this.root);
    this.words = new Set<string>();
  }

  /**
   * add word to dictionary for search
   */
  public addWord(word:string):this {
    if (word) {
      this.words.add(word);
    }
    return this;
  }

  private addWordNodes(word:string):void {
    if (word) {
      for (let i = 1; i < word.length; i++) {
        let str = word.substr(0, i);
        this.addNode(false, str);
      }

      this.addNode(true, word);
    }
  }

  private addNode(inDictionary, word):void {
    let node = this.allNodes.get(word);
    if (!node) {
      node = new TrieNode(inDictionary, word);
    }
    if (inDictionary)
      node.inDictionary = true;
    this.allNodes.set(word, node);
  }

  public build():this {
    this.buildNode();
    this.buildSuffixLink();
    return this;
  }

  private buildNode():void {
    this.words.forEach((word:string) => {
      this.addWordNodes(word);
    });
  }

  private buildSuffixLink():void {
    this.allNodes.forEach((value:TrieNode, key:string) => {
      this.setSuffixLink(key, value);
    });
  }

  private setSuffixLink(value:string, node:TrieNode):void {
    if (!value)
      return;
    let suffixLink = this.root;
    let dictSuffixLink;

    for (let i = value.length - 1; i > 0; i--) {
      let str = value.substring(i, value.length);
      let linkNode = this.allNodes.get(str);
      if (linkNode) {
        if (linkNode.inDictionary)
          dictSuffixLink = linkNode;
        else
          suffixLink = linkNode;
      }
    }

    node.suffixLink = suffixLink;
    if (dictSuffixLink)
      node.dictSuffixLink = dictSuffixLink;
  }

  public search(str:string):SearchResult[] {
    if (!str)
      return [];

    let result:SearchResult[] = [];

    let currentNode = this.root;

    for (let i = 0; i < str.length; i++) {
      if (currentNode && currentNode.value) {
        let childNode = this.allNodes.get(currentNode.value + str[i]);
        while (!(childNode && childNode.value)) {
          currentNode = currentNode.suffixLink;
          childNode = this.allNodes.get(currentNode.value + str[i]);
          if (!currentNode.value)
            break;
        }
        currentNode = childNode;

        if (currentNode && currentNode.inDictionary)
          result.push(new SearchResult(currentNode.value, i + 1 - currentNode.value.length, i));
        if (currentNode && currentNode.dictSuffixLink) {
          result.push(new SearchResult(currentNode.dictSuffixLink.value, i + 1 - currentNode.dictSuffixLink.value.length, i));
        }
      } else {
        currentNode = this.allNodes.get(str[i]);
      }
    }
    return result;
  }

}

class TrieNode {
  inDictionary:boolean;
  value:string;
  suffixLink:TrieNode;
  dictSuffixLink:TrieNode;

  constructor(inDictionary:boolean = false,
              value:string = "",
              suffixLink:TrieNode = null,
              dictSuffixLink:TrieNode = null) {
    this.inDictionary = inDictionary;
    this.value = value;
    this.suffixLink = suffixLink;
    this.dictSuffixLink = dictSuffixLink;
  }

}

export class SearchResult {
  word:string;
  start:number;
  end:number;

  constructor(word:string, start:number, end:number) {
    this.word = word;
    this.start = start;
    this.end = end;
  }
}
