import {AhoCroasickSearch, SearchResult} from "./aho-croasick/AhoCroasickSearch";
import {LetterGrid} from "./LetterGrid";
export class LetterGridSolver {
  search:AhoCroasickSearch;
  letterGrid:LetterGrid;
  words:string[];

  constructor() {
    this.search = new AhoCroasickSearch();
  }

  setWords(words:string[]) {
    this.words = words.map(it => it.toLowerCase());
  }

  setLetterGrid(letterGrid:LetterGrid) {
    this.letterGrid = letterGrid;
  }

  solve():GridSearchResult[] {
    let result:GridSearchResult[] = [];
    this.words.forEach(word => {
      this.search.addWord(word);
    });
    let gridSize = this.letterGrid.matrix.length;

    this.search.build();
    this.letterGrid.getRows()
      .forEach((row, index) => {
        let result1 = this.search.search(row);
        result = this.mergeResult(result, result1.map(it =>
          new GridSearchResult(it, gridSize, false, undefined, index)));
        let result2 = this.search.search(row.split('').reverse().join(''));
        result = this.mergeResult(result, result2.map(it =>
          new GridSearchResult(it, gridSize, true, undefined, index)));
      });

    this.letterGrid.getColumns()
      .forEach((row, index) => {
        let result1 = this.search.search(row);
        result = this.mergeResult(result, result1.map(it =>
          new GridSearchResult(it, gridSize, false, index, undefined)));
        let result2 = this.search.search(row.split('').reverse().join(''));
        result = this.mergeResult(result, result2.map(it =>
          new GridSearchResult(it, gridSize, true, index, undefined)));
      });

    return result;
  }

  private mergeResult(result:GridSearchResult[], result1:GridSearchResult[]):GridSearchResult[] {
    result1.forEach((item:GridSearchResult) => {
      if (!result.find((it:GridSearchResult)=> {
          if (it.word === item.word)
            return true;
          else if (it.word === item.word.split('').reverse().join(''))
            return true;
          return false;
        })) {
        result.push(item);
      }
    });
    return result;
  }

}

export class GridSearchResult {
  word:string;
  startX:number;
  startY:number;
  endX:number;
  endY:number;

  constructor(searchResult:SearchResult, gridSize:number, isReverse:boolean, columnIndex:number, rowIndex:number) {
    this.word = searchResult.word;
    if (columnIndex === undefined) {
      this.startY = rowIndex;
      this.endY = rowIndex;

      if (isReverse) {
        this.startX = gridSize - 1 - searchResult.start;
        this.endX = gridSize - 1 - searchResult.end;
      } else {
        this.startX = searchResult.start;
        this.endX = searchResult.end;
      }
    } else {
      this.startX = columnIndex;
      this.endX = columnIndex;

      if (isReverse) {
        this.startY = gridSize - 1 - searchResult.start;
        this.endY = gridSize - 1 - searchResult.end;
      } else {
        this.startY = searchResult.start;
        this.endY = searchResult.end;
      }
    }
  }
}
