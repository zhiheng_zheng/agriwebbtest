export class LetterGrid {
  value:string;
  matrix:string[][];

  constructor(value:string) {
    this.value = value.toLowerCase();
    this.matrix = [];
    this.value.split('\n').forEach((line:string) => {
      this.matrix.push(line.split(''));
    });
  }

  public isValid():boolean {
    let size = this.matrix.length;
    for (let i = 0;i< this.matrix.length; i++){
      if (this.matrix[i].length != size)
        return false;
    }

    return true;
  }

  public getRows():string[] {
    return this.matrix.map(row => row.join(''));
  }

  public getColumns():string[] {
    let result = [];
    for (let i = 0; i < this.matrix.length; i++) {
      let column = [];
      for (let j = 0; j < this.matrix.length; j++) {
        column.push(this.matrix[j][i]);
      }
      result.push(column.join(''));
    }

    return result;
  }
}
